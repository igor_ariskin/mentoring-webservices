package com.epam.mentoring.dao;

import java.sql.SQLException;

import com.epam.mentoring.types.User;

/**
 * User repository.
 * 
 * @author Igor_Ariskin
 * 
 */
public interface UserRepository {

	/**
	 * Gets user by login and password.
	 * 
	 * @param login
	 * @param password
	 * @return user
	 * @throws SQLException
	 *             in case sql errors.
	 */
	public User getUserByLoginPassword(String login, String password)
			throws SQLException;

}
