package com.epam.mentoring.dao;

import java.util.List;

import com.epam.mentoring.types.Book;

/**
 * Book repository.
 * 
 * @author Igor_Ariskin
 * 
 */
public interface BookRepository {

	/**
	 * Adds new book.
	 * 
	 * @param book
	 *            new book.
	 * @return id of new book.
	 */
	Long addBook(Book book);

	/**
	 * Edits book.
	 * 
	 * @param book
	 *            new book.
	 * @return new book.
	 */
	Book editBook(Book book);

	/**
	 * Removes book.
	 * 
	 * @param id
	 *            book id.
	 * @return true - removed.
	 */
	boolean removeBook(Long id);

	/**
	 * Gets list of all books. Argument sortByFields specify order sort.
	 * 
	 * @param sortByFields
	 *            order sort
	 * @return list of all books
	 */
	List<Book> getAllBooks(String sortByFields);

	/**
	 * Gets book by id.
	 * 
	 * @param id
	 *            book id.
	 * @return book.
	 */
	Book getBook(Long id);
}
