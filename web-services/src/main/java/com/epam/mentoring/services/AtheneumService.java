package com.epam.mentoring.services;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.epam.mentoring.dao.BookRepository;
import com.epam.mentoring.dao.BookRepositoryImpl;
import com.epam.mentoring.types.Book;

/**
 * Atheneum jax-rs service.
 * 
 * @author Igor_Ariskin
 * 
 */
@Path("/atheneum")
public class AtheneumService {

	BookRepository repository = new BookRepositoryImpl();

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Long addBook(Book book) {
		return repository.addBook(book);
	}

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Book editBook(@PathParam(value = "id") Long id, Book book) {
		book.setId(id);
		return repository.editBook(book);
	}

	@DELETE
	@Path("/{id}")
	public boolean removeBook(@PathParam(value = "id") Long id) {
		return repository.removeBook(id);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Book> getAllBooks(
			@DefaultValue("name") @QueryParam(value = "sort") String sortByFields) {
		return repository.getAllBooks(sortByFields);
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Book getBook(@PathParam(value = "id") Long id) {
		return repository.getBook(id);
	}

}
