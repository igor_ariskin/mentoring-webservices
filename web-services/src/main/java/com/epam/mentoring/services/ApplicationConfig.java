package com.epam.mentoring.services;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configuration of jax-ws services.
 * 
 * @author Igor_Ariskin
 * 
 */
@ApplicationPath("/")
public class ApplicationConfig extends Application {

	public Set<Class<?>> getClasses() {
		return new HashSet<Class<?>>(Arrays.asList(AtheneumService.class));
	}
}