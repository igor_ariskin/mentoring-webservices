$(document).ready(function() {
	getAllBook("name");
});

function sort(field, sortName) {
	$("#list_book").find('tbody').empty();
	getAllBook(field);
	$(".dropdown-toggle").html(sortName + '<span class="caret"></span>');
	return false;
}

function getAllBook(data) {
	var url = 'http://localhost:8080/web-services/atheneum/getAll';
	var options = ({
		type : "POST",
		success : function(data) {
			$.each(data, function(index, value) {
				var item = getTemplate(++index, value);
				$("#list_book").find('tbody').append(item);
				addClickBookHandler(item);
			});
		}
	});
	ajaxRequest(url, data, options, false);
}

function getTemplate(index, value) {
	return '<tr name="row" id="'
			+ value.id
			+ '">'
			+ '<td>'
			+ index
			+ '</td>'
			+ '<td><div name="name">'
			+ value.name
			+ '</div></td>'
			+ '<td><div name="author">'
			+ value.author
			+ '</div></td>'
			+ '<td><div name="description">'
			+ value.description
			+ '</div></td>'
			+ '<td><div style="width: 135px;"><button id="'
			+ value.id
			+ '" type="button" name="edit" class="btn btn-warning" style="margin-right: 5px;" onclick="editBook(this)">Edit</button><button id="'
			+ value.id
			+ '" type="button" name="save" class="btn btn-info" style="margin-right: 5px; display:none;" onclick="saveBook(this)">Save</button>'
			+ '<button id="'
			+ value.id
			+ '" type="button" class="btn btn-danger" onclick="deleteBook(this)">Delete</button></div></td></tr>';
}

function deleteBook(button) {
	var url = 'http://localhost:8080/web-services/atheneum/remove';
	var data = button.id;
	var options = ({
		type : "DELETE",
		success : function(data) {
			if (data) {
				showSuccess("Success! The book is removed.");
				deleteRow($("tr#" + button.id));
			} else {
				showError("Error! The book isn't removed.");
			}
		}
	});
	ajaxRequest(url, data, options, false);
}

function saveBook(button) {
	$(button).hide();
	var tr = $("tr#" + button.id);
	tr.find('button[name="edit"]').show();
	var nameInput = tr.find('input[name="edit_name"]');
	var nameDiv = tr.find('div[name="name"]');
	var name = nameInput.val();
	nameInput.remove();

	var authorInput = tr.find('input[name="edit_author"]');
	var authorDiv = tr.find('div[name="author"]');
	var author = authorInput.val();
	authorInput.remove();

	var descriptionInput = tr.find('input[name="edit_description"]');
	var descriptionDiv = tr.find('div[name="description"]');
	var description = descriptionInput.val();
	descriptionInput.remove();

	var url = 'http://localhost:8080/web-services/atheneum/edit';
	var data = ({
		id : button.id,
		name : name,
		author : author,
		description : description
	});
	var options = ({
		type : "POST",
		success : function(data) {
			if (data) {
				showSuccess("Success! Record of the book has changed.");
				nameDiv.append(data.name);
				authorDiv.append(data.author);
				descriptionDiv.append(data.description);
			} else {
				showError("Error! Record of the book hasn't changed.");
			}
		}
	});
	ajaxRequest(url, data, options, true);
}

function editBook(button) {
	$(button).hide();
	var tr = $("tr#" + button.id);
	tr.find('button[name="save"]').show();
	var nameDiv = tr.find('div[name="name"]');
	var name = nameDiv.text();
	nameDiv.empty();
	nameDiv.append("<input name='edit_name' value='" + name + "'>");

	var authorDiv = tr.find('div[name="author"]');
	var author = authorDiv.text();
	authorDiv.empty();
	authorDiv.append("<input name='edit_author' value='" + author + "'>");

	var descriptionDiv = tr.find('div[name="description"]');
	var description = descriptionDiv.text();
	descriptionDiv.empty();
	descriptionDiv.append("<input name='edit_description' value='"
			+ description + "'>");
}

function deleteRow(tr) {
	tr.css("background-color", "#FF3700");
	tr.fadeOut(400, function() {
		tr.remove();
	});
}

function addClickBookHandler(item) {
	var id = $(item).attr('id');
	$('tr#' + id).dblclick(function() {
		var url = 'http://localhost:8080/web-services/atheneum/get';
		var data = id;
		var options = ({
			type : "POST",
			success : function(book) {
				if (book) {
					$(".modal-body").empty();
					$(".modal-body").append(getModalBody(book));
					$('#myModal').modal('show');
				}
			}
		});
		ajaxRequest(url, data, options, false);
	});
}

function getModalBody(book) {
	return '<table><tbody><tr><td style="font-weight: bold; vertical-align: top;">Name: </td><td>'
			+ book.name
			+ '</td></tr><tr><td style="font-weight: bold; vertical-align: top;">Author: </td><td>'
			+ book.author
			+ '</td></tr><tr><td style="font-weight: bold; vertical-align: top;">Description: </td><td>'
			+ book.description + '</td></tr></tbody></table>';
}

function addBook() {
	var url = 'http://localhost:8080/web-services/atheneum/add';
	var data = ({
		name : $('#add_name').val(),
		author : $('#add_author').val(),
		description : $('#add_description').val()
	});
	var options = ({
		type : "PUT",
		success : function(id) {
			if (id) {
				showSuccess("Success! The book is added.");
				$('input').val('');
				var newBook = ({
					id : id,
					name : data.name,
					author : data.author,
					description : data.description
				});
				var item = getTemplate(1, newBook);
				$("#list_book").find('tbody').prepend(item);
				addClickBookHandler(item);
			} else {
				showError("Error! The book isn't added.");
			}
		}
	});
	ajaxRequest(url, data, options, true);
}

function showSuccess(text) {
	var msg = $('.alert-success.myalert');
	msg.append(text);
	msg.show();
	setTimeout(function() {
		msg.hide();
		msg.empty();
	}, 3000);
}

function showError(text) {
	var msg = $('.alert-danger.myalert');
	msg.append(text);
	msg.show();
	setTimeout(function() {
		msg.hide();
		msg.empty();
	}, 3000);
}

function ajaxRequest(url, data, extOptions, json) {
	var defaults = ({
		timeout : 20000,
		contentType : "application/json",
		beforeSend : function() {
			$("div.msgload").show();
		},
		complete : function() {
			$("div.msgload").hide();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			var rateLimitError = jQuery.parseJSON(jqXHR.responseText);
			if (rateLimitError.rateLimit == true) {
				showModalRateLimit(parseInt(rateLimitError.resetTime));
			} else if (jqXHR.readyState != 0) {
				$("div.msgErrorOnLoad").text(
						" " + textStatus + ": " + errorThrown);
				$("div.msgErrorOnLoad").show();
			}
		}
	});
	var options = $.extend(defaults, extOptions);
	$.ajaxSetup({
		cache : false
	});
	if (json) {
		data = (data != null) ? JSON.stringify(data) : null;
	}
	$.ajax({
		timeout : options.timeout,
		url : url,
		type : options.type,
		dataType : 'json',
		contentType : options.contentType,
		data : data,
		beforeSend : options.beforeSend,
		complete : options.complete,
		error : options.error,
		success : options.success
	});
}