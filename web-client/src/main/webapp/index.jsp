<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon"
	href="http://getbootstrap.com/assets/ico/favicon.ico">

<title>Signin Template for Bootstrap</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/protected/css/bootstrap.min.css">
<!-- Custom styles for this template -->
<link href="${pageContext.request.contextPath}/protected/css/signin.css"
	rel="stylesheet">

</head>

<body>
	<div class="container">
		<%
			String error = (String) request.getAttribute("error");
			if (error != null) {
		%>
		<div class="alert alert-danger"><%=error%></div>
		<%
			}
		%>
		<form class="form-signin" role="form" action="./" method="post">
			<h2 class="form-signin-heading">Please sign in</h2>
			<input name="login" type="text" class="form-control"
				placeholder="Login" required="required" autofocus=""> <input
				name="password" type="password" class="form-control"
				placeholder="Password" required="required"> <label
				class="checkbox"> <input type="checkbox" value="remember-me">
				Remember me
			</label>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign
				in</button>
		</form>

	</div>
	<!-- /container -->
</body>
</html>