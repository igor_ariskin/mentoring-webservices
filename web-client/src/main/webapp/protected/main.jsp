<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon"
	href="http://getbootstrap.com/assets/ico/favicon.ico">


<!-- Latest compiled and minified CSS -->


<!-- Optional theme -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/protected/css/bootstrap-theme.min.css">

<title>Dashboard Template for Bootstrap</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/protected/css/bootstrap.min.css">

</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<div class="alert alert-success myalert" style="display: none;"></div>
				<div class="alert alert-danger myalert" style="display: none;"></div>
				<h1 class="page-header">Atheneum</h1>

				<div class="row placeholders">
					<div class="row">
						<div class="col-lg-3">
							<div class="input-group">
								<label>Title</label> <input id="add_name" type="text"
									class="form-control">
							</div>
							<!-- /input-group -->
						</div>
						<!-- /.col-lg-6 -->
						<div class="col-lg-3">
							<div class="input-group">
								<label>Author</label> <input id="add_author" type="text"
									class="form-control">
							</div>
							<!-- /input-group -->
						</div>
						<div class="col-lg-3">
							<div class="input-group">
								<label>Description</label> <input id="add_description"
									type="text" class="form-control">
							</div>
							<!-- /input-group -->
						</div>
						<div class="col-lg-3">
							<button type="button" class="btn btn-primary"
								style="margin-top: 24px;" onclick="addBook()">Add</button>
							<!-- /input-group -->
						</div>
						<!-- /.row -->
					</div>

					<h2 class="sub-header" style="margin-top: 50px;">List of books</h2>
					<div class="btn-group">
						<button type="button" class="btn btn-primary dropdown-toggle"
							data-toggle="dropdown">
							Sort by <span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu" style="cursor: pointer;">
							<li><a name="sort" onclick="sort('name', 'Title')">Title</a></li>
							<li><a name="sort"
								onclick="sort('description', 'Description')">Description</a></li>
							<li><a name="sort"
								onclick="sort('name, description', 'Title&Description')">Title&Description</a></li>
						</ul>
					</div>
					<div class="table-responsive">
						<table class="table table-striped" id="list_book">
							<thead>
								<tr>
									<th>#</th>
									<th>Title</th>
									<th>Author</th>
									<th>Description</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true"
			style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Information about
							book</h4>
					</div>
					<div class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

		<script lang="javascript"
			src="${pageContext.request.contextPath}/protected/js/jquery-2.1.1.js"></script>
		<script lang="javascript"
			src="${pageContext.request.contextPath}/protected/js/bootstrap.js"></script>
		<script lang="javascript"
			src="${pageContext.request.contextPath}/protected/js/webclient.js"></script>
		<script lang="javascript"
			src="${pageContext.request.contextPath}/protected/js/json2.js"></script>
		<script lang="javascript"
			src="${pageContext.request.contextPath}/protected/js/bootstrap-modal.js"></script>
		<script lang="javascript"
			src="${pageContext.request.contextPath}/protected/js/bootstrap-modalmanager.js"></script>
</body>
</html>